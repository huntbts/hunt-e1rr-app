import json
import mysql.connector 
from mysql.connector import Error
import boto3
import base64
from botocore.exceptions import ClientError
import os
    
region_name = os.environ['regionName']
secret_name = os.environ['secretName']
  
 # If secret is undefined then connect to secret manager and retrieve secrets, otherwise use secret
try:
    secret
except NameError:
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region_name)
    get_secret_value_response = client.get_secret_value(SecretId=secret_name)

    # Decrypts secret using the associated KMS CMK.
    # Depending on whether the secret is a string or binary, one of these fields will be populated.
    if 'SecretString' in get_secret_value_response:
        secret = json.loads(get_secret_value_response['SecretString'])
    else:
        secret = json.loads(base64.b64decode(get_secret_value_response['SecretBinary']))

lvDabataseUser = secret['username']
lvDatabasePswd = secret['password']
lvDatabaseHost = secret['host']
lvDatabaseName = secret['dbname']

  # If Connection is not defined then connect to database else use same connection, this will save some memory for the lambda which will cost less money
try:
    if(connection):
       if (connection.is_connected() is False):
           connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)
except NameError:
    connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)

def lambda_handler(event, context):
    #envName = 'E1PROD'
    envName = event["queryStringParameters"]['envName']
    response = 'Environment ' + envName + ' is not ready'

    try:
        cursor = connection.cursor()
        cursor.callproc('GetRoundRobinNextSite', [envName])
        connection.commit()

        for record in cursor.stored_results():
            row = record.fetchone()
            if row:
                response = row[3]

    except mysql.connector.Error as error:
        response = "Failed to execute stored procedure: {}".format(error)

  # return a properly formatted JSON object
    return {
    'statusCode': 302,
    'headers':{
        'location':response
    }
    }   