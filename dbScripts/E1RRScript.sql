
use e1rrdb;

CREATE TABLE `E1RoundRobinCtrl` (
  `PKID` int unsigned NOT NULL AUTO_INCREMENT,
  `environmentID` int NOT NULL,
  `lastUsedSiteID` int NOT NULL,
  PRIMARY KEY (`PKID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Environments` (
  `PKID` int NOT NULL AUTO_INCREMENT,
  `environmentName` varchar(150) NOT NULL,
  `descr` varchar(250) DEFAULT NULL,
  `isEnabled` tinyint DEFAULT '1',
  `dtsCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `dtsModified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PKID`),
  UNIQUE KEY `environmentName_UNIQUE` (`environmentName`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Sites` (
  `PKID` int NOT NULL AUTO_INCREMENT,
  `environmentID` int NOT NULL,
  `siteName` varchar(100) NOT NULL,
  `siteUrl` varchar(250) NOT NULL,
  `isEnabled` tinyint NOT NULL DEFAULT '1',
  `dtsCreated` datetime DEFAULT CURRENT_TIMESTAMP,
  `dtsModified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PKID`),
  UNIQUE KEY `unique_env_siteName` (`environmentID`,`siteName`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `Environments_AddNew`(
	  IN pEnvName varchar(150)
    , IN pEnvDescr varchar(250)
    , IN pIsEnabled tinyint(4)
)
BEGIN
	DECLARE PKID  INT unsigned DEFAULT 0; 
    DECLARE p1 VARCHAR(1000) DEFAULT '';
    DECLARE p2 VARCHAR(1000) DEFAULT '';


	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
        GET DIAGNOSTICS CONDITION 1 p1 = RETURNED_SQLSTATE, p2 = MESSAGE_TEXT;
        SELECT 0 AS retSuccess, (SELECT CONCAT(p1, ':', p2)) AS returnMessage, PKID AS recordID;
        ROLLBACK;
    END;
    
    START TRANSACTION;
	INSERT INTO `e1rrdb`.`Environments`
    (
		 `environmentName`
        , `descr`
        , `isEnabled`
    )
    VALUES
    (
		  pEnvName
		, pEnvDescr
		, pIsEnabled
    );
		
    COMMIT;       
	SET   PKID = (SELECT LAST_INSERT_ID());

    SELECT 1 AS retSuccess, '' AS returnMessage, PKID AS recordID;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `Environments_GetFullList`()
BEGIN
	SELECT 
		  e.*
    FROM Environments e;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `Environments_UpdateByID`(
	  IN pEnvName varchar(150)
    , IN pEnvDescr varchar(250)
    , IN pIsEnabled tinyint(4)
    , IN pEnvID INT(11)
)
BEGIN
	DECLARE affectedRecords INT DEFAULT 0;
    DECLARE retMsg VARCHAR(1000) DEFAULT '';
    DECLARE p1 VARCHAR(1000) DEFAULT '';
    DECLARE p2 VARCHAR(1000) DEFAULT '';
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
        GET DIAGNOSTICS CONDITION 1 p1 = RETURNED_SQLSTATE, p2 = MESSAGE_TEXT;
        SELECT 0 AS retSuccess, (SELECT CONCAT(p1, ':', p2)) AS returnMessage, pEnvID AS recordID;
        ROLLBACK;
    END;
    

    -- Set to Null in case name is empty string in Order to enforce constrain for not allowing null
	-- SET pEnvName = SetToNullIfBlank(pEnvName);

    START TRANSACTION;
    
	UPDATE Environments AS e
		SET  e.environmentName = pEnvName
        , e.descr = pEnvDescr
        , e.isEnabled = pIsEnabled
        , e.dtsModified = NOW()
	WHERE e.PKID = pEnvID;
    
    COMMIT;     
    
    SET affectedRecords = (FOUND_ROWS());
    
    SELECT 'No records were found to update' INTO retMsg FROM DUAL WHERE affectedRecords <= 0;
    
    SELECT (affectedRecords > 0) AS retSuccess, retMsg AS returnMessage, pEnvID AS recordID, affectedRecords AS affectedRecords;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `GetAllEnvironments`()
BEGIN
	
    SELECT  
		  e.PKID as environmentID
        , e.environmentName
        , e.descr
        , ifnull(e.isEnabled,0) as isEnabled
        , ifnull(e.dtsCreated,'') as dtsCreated
        , ifnull(e.dtsModified,'') as dtsModified
    FROM Environments e;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `GetAllSites`()
BEGIN

	SELECT 
		  ifnull(s.PKID,-1) as siteID
        , ifnull(e.PKID,-1) as environmentID
        , ifnull(e.environmentName,'') as environmentName
        , ifnull(s.Sitename,'') as siteName
        , ifnull(s.siteUrl,'') as siteUrl
        , ifnull(s.isEnabled,0) as isSiteEnabled
        , ifnull(s.dtsCreated,'') as siteDtsCreated
        , ifnull(s.dtsModified,'') as siteDtsModified
        , ifnull(e.isEnabled,0) as isEnvironmentEnabled
        , ifnull(e.dtsCreated,'') as envDtsCreated
        , ifnull(e.dtsModified,'') as envDtsModified
		, CONCAT('https://z7tdl2ncle.execute-api.us-east-1.amazonaws.com/dev?envName=',ifnull(e.environmentName,''))  as roundRobinLink        
    FROM Environments e
    LEFT OUTER JOIN Sites s
		ON s.environmentID = e.PKID;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `GetRoundRobinNextSite`(env_name VARCHAR(100))
BEGIN
	DECLARE lvOrderNumOfLastSent INT;
    DECLARE lvNextSiteID INT;
    DECLARE lvLastUsedCtrlPKID INT;
    DECLARE lvEnvironmentID INT;
    DECLARE lvMaxSiteID INT;
    
    -- Getting EnvID From parameter env Name
    SELECT MAX(PKID) INTO lvEnvironmentID FROM Environments WHERE isEnabled != 0 AND environmentName = env_name;
    -- Getting MaxSiteID For Selected environment
    SELECT MAX(PKID) INTO lvMaxSiteID FROM Sites WHERE isEnabled != 0 AND environmentID = lvEnvironmentID;
    
    -- If environmentID is not null we insert the record in the E1RoundRobinCtrl Table
    -- if MaxSiteID is NULL then we insert zero as maxSiteID
    IF lvEnvironmentID IS NOT NULL THEN
		INSERT INTO E1RoundRobinCtrl (environmentID, lastUsedSiteID)
		SELECT lvEnvironmentID, IFNULL(lvMaxSiteID,0)
        WHERE NOT EXISTS (SELECT * FROM E1RoundRobinCtrl WHERE environmentID = lvEnvironmentID LIMIT 1);
    END IF;
    
	-- WE SELECT the PKID from Ctrl table and the lastUsedSite ID as per the environment selected
    -- we save the corresponding values in the variables @lvLastUsedCtrlPKID, @lvOrderNumOfLastSent
	SELECT 
		c.PKID, s.PKID INTO lvLastUsedCtrlPKID, lvOrderNumOfLastSent
    FROM E1RoundRobinCtrl c
    INNER JOIN Environments e
		ON c.environmentID = e.PKID
        AND e.isEnabled != 0
        AND e.PKID = lvEnvironmentID
	INNER JOIN Sites s
		ON s.environmentID = e.PKID
        AND s.IsEnabled != 0
        AND (s.PKID = c.lastUsedSiteID  OR c.lastUsedSiteID = 0)
	LIMIT 1;
        
	-- If the  @lvOrderNumOfLastSent >= @lvMaxSiteID then we retrieve the minimum PKID  from the enabled records,
    -- Otherwise we retrieve the minimum PKID for the enabled records that are greater than @lvOrderNumOfLastSent
    -- Ww save the value in the @lvNextSiteID variable
	IF lvOrderNumOfLastSent >= lvMaxSiteID THEN
		SELECT MIN(s.PKID) INTO lvNextSiteID
        FROM Sites s
        WHERE s.isEnabled != 0
        AND s.environmentID = lvEnvironmentID;
    ELSE
		SELECT MIN(s.PKID) INTO lvNextSiteID
        FROM Sites s
        WHERE s.PKID > lvOrderNumOfLastSent
        AND s.isEnabled != 0
        AND s.environmentID = lvEnvironmentID;
    END IF;
    
    
    -- we update the lastUsedSiteID in the E1RoundRobinCtrl table as per the new value stored in the @lvNextSiteID variable
    -- the following variables must not be null @lvEnvironmentID, @lvMaxSiteID
    UPDATE E1RoundRobinCtrl
		SET lastUsedSiteID = lvNextSiteID
    WHERE PKID = lvLastUsedCtrlPKID
    AND lvEnvironmentID IS NOT NULL
    AND lvMaxSiteID IS NOT NULL;
    
    -- finally we retrieve the next Site information to be served
    SELECT s.* , lvNextSiteID, lvLastUsedCtrlPKID, lvOrderNumOfLastSent, lvEnvironmentID, lvMaxSiteID
    FROM Sites s
    WHERE PKID = lvNextSiteID;
    

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `Sites_AddNew`(
	  IN pEnvId INT(11)
    , IN pSiteName varchar(100)
    , IN pSiteUrl varchar(250)
    , IN pIsEnabled tinyint(4)
)
BEGIN
	DECLARE PKID  INT unsigned DEFAULT 0; 
    DECLARE p1 VARCHAR(1000) DEFAULT '';
    DECLARE p2 VARCHAR(1000) DEFAULT '';

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
        GET DIAGNOSTICS CONDITION 1 p1 = RETURNED_SQLSTATE, p2 = MESSAGE_TEXT;
        SELECT 0 AS retSuccess, (SELECT CONCAT(p1, ':', p2)) AS returnMessage, PKID AS recordID;
        ROLLBACK;
    END;
    
    SET pSiteName = SetToNullIfBlank(pSiteName);
    SET pSiteUrl = SetToNullIfBlank(pSiteUrl);
    
    START TRANSACTION;
	INSERT INTO `e1rrdb`.`Sites`
    (
		 `environmentID`
        , `siteName`
        , `siteUrl`
        , `isEnabled`
    )
    VALUES
    (
		  pEnvId
		, pSiteName          
		, pSiteUrl
		, pIsEnabled
    );
    COMMIT;  		
	SET   PKID = (SELECT LAST_INSERT_ID());

    SELECT (PKID > 0) AS retSuccess, '' AS returnMessage, PKID AS recordID;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`admin`@`%` PROCEDURE `Sites_UpdateByID`(
	  IN pEnvId INT(11)
    , IN pSiteName varchar(100)
    , IN pSiteUrl varchar(250)
    , IN pIsEnabled tinyint(4)
    , IN pSiteID INT(11)
)
BEGIN
	DECLARE affectedRecords INT DEFAULT 0;
    DECLARE retMsg VARCHAR(1000) DEFAULT '';
    DECLARE p1 VARCHAR(1000) DEFAULT '';
    DECLARE p2 VARCHAR(1000) DEFAULT '';

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
        GET DIAGNOSTICS CONDITION 1 p1 = RETURNED_SQLSTATE, p2 = MESSAGE_TEXT;
        SELECT 0 AS retSuccess, (SELECT CONCAT(p1, ':', p2)) AS returnMessage, pSiteID AS recordID;
        ROLLBACK;
    END;
    
    SET pSiteName = SetToNullIfBlank(pSiteName);
    SET pSiteUrl = SetToNullIfBlank(pSiteUrl);

    START TRANSACTION;
        
    UPDATE Sites AS s
		SET  s.environmentID = pEnvID
        , s.siteName = pSiteName
        , s.siteUrl = pSiteUrl
        , s.isEnabled = pIsEnabled
        , s.dtsModified = NOW()
	WHERE s.PKID = pSiteID;
    
    COMMIT;     

	SET affectedRecords = (FOUND_ROWS());
    
    SELECT 'No records were found to update' INTO retMsg FROM DUAL WHERE affectedRecords <= 0;
    
    SELECT (affectedRecords > 0) AS retSuccess, retMsg AS returnMessage, pSiteID AS recordID, affectedRecords AS affectedRecords;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`admin`@`%` FUNCTION `SetToNullIfBlank`(pStringValue varchar(500)) RETURNS varchar(500) CHARSET utf8mb4
BEGIN

IF length(trim(pStringValue)) <= 0 THEN
	return null;
ELSE
	return pStringValue;
END IF;

END$$
DELIMITER ;

