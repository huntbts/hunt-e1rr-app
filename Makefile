# Invoke Runway
.EXPORT_ALL_VARIABLES:
DEPLOY_ENVIRONMENT=dev

install:
	poetry install

plan:
	poetry run runway plan

deploy:
	poetry run runway deploy

plan-dev:
	DEPLOY_ENVIRONMENT=dev poetry run runway plan

plan-uat:
	DEPLOY_ENVIRONMENT=uat poetry run runway plan

plan-prod:
	DEPLOY_ENVIRONMENT=prod poetry run runway plan

deploy-dev: install
	DEPLOY_ENVIRONMENT=dev poetry run runway deploy

deploy-uat: install
	DEPLOY_ENVIRONMENT=uat poetry run runway deploy

deploy-prod: install
	DEPLOY_ENVIRONMENT=prod poetry run runway deploy


