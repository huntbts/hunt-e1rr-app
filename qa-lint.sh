#!/bin/sh
set +e

alias discover-linter='find . -type f -name "*.py" -not -path "./venv/*"';
alias discover-style-checker='find . -type f -name "*.py" -not -path "./venv/*"';

last=0

echo "testing with cfn-lint"
cfn-lint cloudformation/*.yaml

echo "testing with pylint"
discover-linter | xargs python -m pylint
last=$(($last|$?))

echo "testing with flake8"
discover-style-checker | xargs python -m flake8
last=$(($last|$?))

set -e

if [ $last -ne 0 ]
then
  echo "Failure!"
  exit 1
else
  echo "All tests successful!"
fi