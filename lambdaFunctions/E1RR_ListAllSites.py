import json
import mysql.connector 
from mysql.connector import Error
import boto3
import base64
from botocore.exceptions import ClientError
import os

region_name = os.environ['regionName']
secret_name = os.environ['secretName']

def lambda_handler(event, context):

    response = ''
    wasSuccessful = True
    returnMessage = ''
    payload1 = []
    payload2 = [] 
    
    try:
        
        session = boto3.session.Session()
        client = session.client(service_name='secretsmanager', region_name=region_name)
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
        
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = json.loads(get_secret_value_response['SecretString'])
        else:
            secret = json.loads(base64.b64decode(get_secret_value_response['SecretBinary']))

        lvDabataseUser = secret['username']
        lvDatabasePswd = secret['password']
        lvDatabaseHost = secret['host']
        lvDatabaseName = secret['dbname']            
        
        connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)
        
        cursor = connection.cursor()
        #query = "SELECT  e.PKID as environmentID, e.environmentName, e.descr, ifnull(e.isEnabled,0) as isEnabled, ifnull(e.dtsCreated,'') as dtsCreated, ifnull(e.dtsModified,'') as dtsModified   FROM Environments e"
        cursor.callproc('GetAllEnvironments')
        for records in cursor.stored_results():
            allRows = records.fetchall()
        content = {}
        for row in allRows:
            content = {'EnvironmentID': row[0], 'EnvironmentName': row[1], 'Description': row[2], 'IsEnabled': row[3], 'DtsCreated': row[4], 'DtsModified': row[5]}
            payload1.append(content)
            content = {}

        cursor.callproc('GetAllSites')
        for records in cursor.stored_results():
            allRows = records.fetchall()
        content = {}
        for row in allRows:
            content = {'SiteID': row[0], 'EnvironmentID': row[1], 'EnvironmentName': row[2], 'SiteName': row[3], 'SiteUrl': row[4], 'IsSiteEnabled': row[5],'SiteDtsCreated': row[6], 'SiteDtsModified': row[7], 'IsEnvironmentEnabled': row[8], 'EnvDtsCreated': row[9], 'EnvDtsModified': row[10], 'RoundRobinLink': row[11]}
            payload2.append(content)
            content = {}
    except mysql.connector.Error as error:
        returnMessage = "Failed to execute stored procedure: {}".format(error)
        wasSuccessful = False
    finally:
        if(cursor):
            cursor.close()
        if(connection):
            connection.close()        

    response = json.dumps({'WasSuccessful': wasSuccessful,'ReturnMessage': returnMessage,'Environments': payload1, 'Sites': payload2})
    # return a properly formatted JSON object
    return {
    'statusCode': 200,
    'body': response
    }               



    