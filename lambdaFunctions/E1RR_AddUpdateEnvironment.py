import json
import mysql.connector 
from mysql.connector import Error
import boto3
import base64
from botocore.exceptions import ClientError
import os

region_name = os.environ['regionName']
secret_name = os.environ['secretName']
  
 # If secret is undefined then connect to secret manager and retrieve secrets, otherwise use secret
try:
    secret
except NameError:
    session = boto3.session.Session()
    client = session.client(service_name='secretsmanager', region_name=region_name)
    get_secret_value_response = client.get_secret_value(SecretId=secret_name)

    # Decrypts secret using the associated KMS CMK.
    # Depending on whether the secret is a string or binary, one of these fields will be populated.
    if 'SecretString' in get_secret_value_response:
        secret = json.loads(get_secret_value_response['SecretString'])
    else:
        secret = json.loads(base64.b64decode(get_secret_value_response['SecretBinary']))

lvDabataseUser = secret['username']
lvDatabasePswd = secret['password']
lvDatabaseHost = secret['host']
lvDatabaseName = secret['dbname']


  # If Connection is not defined then connect to database else use same connection, this will save some memory for the lambda which will cost less money
try:
    if(connection):
       if (connection.is_connected() is False):
           connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)
    else:
        connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)           
except NameError:
    connection = mysql.connector.connect(host=lvDatabaseHost, database=lvDatabaseName, user=lvDabataseUser, password=lvDatabasePswd)

def lambda_handler(event, context):

    response = ''
    wasSuccessful = 0
    returnMessage = ''
    payload1 = []
    payload2 = []
    lvAction = ''

    try:
        cursor = connection.cursor()
        
        bodyJson = json.loads(event['body'])
     
        lvEnvId = bodyJson['EnvironmentID']
        lvEnvName = bodyJson['EnvironmentName']
        lvEnvDescription = bodyJson['Description']
        lvEnvEnabled = int(bodyJson['IsEnabled'])
        
        if lvEnvId <= 0:
            # Adding New Environment
            lvAction = 'Adding New Environment'
            cursor.callproc('Environments_AddNew',[lvEnvName, lvEnvDescription, lvEnvEnabled])
            
            for records in cursor.stored_results():
                allRows = records.fetchall()
                
            for row in allRows:
                wasSuccessful = row[0]
                returnMessage = row[1]        

            
        else:
            #updating environment
            lvAction = 'Updating Environment'
            cursor.callproc('Environments_UpdateByID',[lvEnvName, lvEnvDescription, lvEnvEnabled, lvEnvId])
            
            for records in cursor.stored_results():
                allRows = records.fetchall()
                
            for row in allRows:
                wasSuccessful = row[0]
                returnMessage = row[1] 


    except mysql.connector.Error as error:
        returnMessage = "Failed to execute stored procedure: {}".format(error)
        wasSuccessful = 0
        
    response = json.dumps({'WasSuccessful': wasSuccessful,'ReturnMessage': returnMessage,'Action':lvAction })
    
  # return a properly formatted JSON object
    return {
    'statusCode': 200,
    'body': response
    }
    