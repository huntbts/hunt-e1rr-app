# CHANGELOG for E1RR application

## 2023-11-17 - Jake Skipper

- Repo modernization.

## 2021-3-1 - Akram Husain

- Added paramatarization to support code promtion through the different environments
- Added bitbucket linting
- Added changelog
